import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/feed',
    name: 'FeedMain',
    component: () => import(/* webpackChunkName: "feed" */ '../views/Feed/Main.vue'),
    children: [
      {
        path: '/',
        name: 'Feed',
        component: () => import(/* webpackChunkName: "feed" */ '../views/Feed/Feed.vue'),
      },
    ],
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
