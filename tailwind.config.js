module.exports = {
    theme: {
      colors: {
        darkBlue: '#2B2D42',
        lightBlue: '#8D99AE',
        white: '#EDF2F4',
        darkRed: '#D90429',
        lightRed: '#EF233C',
      }
    }
  }